"use client";

import { useState, useEffect } from "react";
import BarChartComponent from "../components/BarChartComponent";
import ChartContainer from "../components/ChartContainer";

const DynamicCharts = () => {
  const [clicked, setClicked] = useState(false);
  const [barValues, setBarValues] = useState([0, 0, 0, 0]);
  const [animationRunning, setAnimationRunning] = useState(false);
  const [errorMessages, setErrorMessages] = useState("");

  const handleClick = () => setAnimationRunning(true);

  // Delay after clicking "Generate Charts" till the animation is done
  useEffect(() => {
    if (animationRunning) {
      setTimeout(() => {
        setClicked(true);
        setAnimationRunning(false);
      }, 1000);
    }
  }, [animationRunning]);

  const handleChange = (index, value) => {
    const enteredValue = value;
    // Validating the entered value to be between 0 and 1000
    if (enteredValue >= 0 && enteredValue <= 1000) {
      setErrorMessages("");
      setBarValues((prevValues) => {
        const newValues = [...prevValues];
        newValues[index] = enteredValue;
        return newValues;
      });
    } else {
      // Setting the entered value to 0 in case of invalid input
      setBarValues((prevValues) => {
        const newValues = [...prevValues];
        newValues[index] = 0;
        return newValues;
      });
      // Display error message in case of invalid input
      setErrorMessages(() => {
        const newMessages = "Please enter a value between 1 and 1000.";
        return newMessages;
      });
    }
  };

  // data array of objects will be passed to the BarChartComponent with the bar values
  const data = [
    {
      name: "A",
      value: barValues[0],
    },
    {
      name: "B",
      value: barValues[1],
    },
    {
      name: "C",
      value: barValues[2],
    },
    {
      name: "D",
      value: barValues[3],
    },
  ];

  return (
    <>
      {/* Conditional rendering the input fields and the Bar Charts when user clicks on "Generate Charts" */}
      {clicked ? (
        <div className="w-full">
          <span className="font-bold text-md sm:text-2xl lg:text-3xl flex justify-center items-center my-4">
            Enter a number between 1 and 1000
          </span>
          <div className="flex flex-wrap justify-center mb-8">
            {barValues.map((value, index) => (
              <input
                key={index}
                type="number"
                min="0"
                max="1000"
                step="50"
                placeholder="1-1000"
                onChange={(e) => handleChange(index, e.target.value)}
                className="w-20 sm:w-24 m-1 sm:mr-4 border border-black rounded-md p-2 bg-transparent"
              />
            ))}
            {errorMessages ? (
              <span className="text-red-500 w-full flex justify-center items-center mt-2 bottom-0 left-0">
                {errorMessages}
              </span>
            ) : (
              ""
            )}
          </div>
          <ChartContainer>
            <BarChartComponent data={data} />
          </ChartContainer>
        </div>
      ) : (
        <div className="flex justify-center items-start h-[90vh]">
          <button
            onClick={handleClick}
            className={`mt-[70px] w-44 ${
              animationRunning ? "w-80 sm:w-96" : ""
            } transition-width duration-500 border border-black rounded-md p-2`}
          >
            Generate Charts
          </button>
        </div>
      )}
    </>
  );
};

export default DynamicCharts;
