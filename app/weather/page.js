"use client";

import { useEffect, useState } from "react";
import axios from "axios";
import LineChartComponent from "../components/LineChartComponent";
import { MoonLoader } from "react-spinners";
import ChartContainer from "../components/ChartContainer";

export default function page() {
  const [weatherData, setWeatherData] = useState({});
  const [loading, setLoading] = useState(false);

  // fetching weather data from API
  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      const position = await getCurrentPosition();
      const response = await axios.get(
        `https://api.open-meteo.com/v1/forecast?latitude=${position.coords.latitude}&longitude=${position.coords.longitude}&daily=temperature_2m_max,temperature_2m_min`
      );
      setWeatherData(response.data);
      setLoading(false);
    };

    // fetching longitude and latitude of the user
    const getCurrentPosition = () => {
      if (navigator.geolocation) {
        return new Promise((resolve, reject) => {
          navigator.geolocation.getCurrentPosition(resolve, reject);
        });
      } else {
        throw new Error("Geolocation is not supported by this browser");
      }
    };

    fetchData();
  }, []);
  // Mapping the API response to create a data array of objects with the structure: { date:..., minTemp: ..., maxTemp: ... }
  // An example of the api response is given in the README file
  const data = weatherData.daily?.time?.map((date, index) => ({
    date,
    minTemp: weatherData.daily.temperature_2m_min[index],
    maxTemp: weatherData.daily.temperature_2m_max[index],
  }));

  return (
    <>
      {loading ? (
        <div className="flex flex-col justify-center items-center h-screen">
          <h1 className="font-bold mb-2">Loading weather data..</h1>
          <MoonLoader loading={loading} size={100} color="black" />
        </div>
      ) : (
        <>
          <span className="font-bold text-md sm:text-2xl lg:text-3xl flex justify-center items-center mt-8 mb-10 sm:my-4">
            Discover the 7-day forecast for your location
          </span>
          <ChartContainer>
            <LineChartComponent data={data} />
          </ChartContainer>
        </>
      )}
    </>
  );
}
