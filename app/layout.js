import "./globals.css";
import Nav from "./components/Nav";
import { Tektur } from "next/font/google";
import Favicon from "/public/favicon.ico";

const tektur = Tektur({ subsets: ["latin"] });

export const metadata = {
  title: "HarvestAi Assessment",
  description: "Assessment including two main features built by Next.js",
  icons: [{ rel: "icon", url: Favicon.src }],
};

export default function RootLayout({ children }) {
  return (
    <html className="bg-[#F7EBD9]" lang="en">
      <body className={tektur.className}>
        <Nav />
        {children}
      </body>
    </html>
  );
}
