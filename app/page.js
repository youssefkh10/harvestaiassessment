import HeroGrid from "./components/HeroGrid";

export default function Home() {
  return (
    <main className="min-h-screen px-8">
      <h1 className="font-bold text-center text-2xl sm:text-3xl md:text-4xl mt-4 pb-24 mb-6 sm:mb-12">
        HarvestAi Assessment
      </h1>
      <HeroGrid />
    </main>
  );
}
