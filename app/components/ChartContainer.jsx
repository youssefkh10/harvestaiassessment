// Container for a chart component of any type
const ChartContainer = ({ children }) => {
  return (
    <div className="flex flex-col items-center justify-center m-4 sm:m-8 border border-slate-900  rounded-xl h-[70vh]">
      {children}
    </div>
  );
};

export default ChartContainer;
