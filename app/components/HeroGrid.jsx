import Link from "next/link";
import Image from "next/image";

const HeroGrid = () => {
  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 gap-6 lg:gap-10">
      <div className="m-2 sm:m-4 md:m-3 lg:m-2 border border-black p-2 sm:p-4 rounded-md transition-transform hover:scale-105">
        <Link className="flex flex-col items-center" href="/weather">
          <div className="w-1/3">
            <Image
              src="/weather.png"
              layout="responsive"
              width={300}
              height={300}
              className="mb-6 rounded-full"
              alt="Weather"
            />
          </div>
          <h2 className="text-xl font-semibold mb-1">Weather Forecast</h2>
          <p className="text-sm text-gray-600 text-center">
            Get your weather forecast for the next 7 days
          </p>
        </Link>
      </div>
      <div className="m-2 sm:m-4 md:m-3 lg:m-2 border border-black p-2 sm:p-4 rounded-md transition-transform hover:scale-105">
        <Link className="flex flex-col items-center" href="/chart">
          <div className="w-1/3">
            <Image
              src="/charts.png"
              layout="responsive"
              width={300}
              height={300}
              className="mb-6 rounded-full"
              alt="Charts"
            />
          </div>
          <h2 className="text-xl font-semibold mb-1">Dynamic Bar Chart</h2>
          <p className="text-sm text-gray-600 text-center">
            Generate dynamic bar charts
          </p>
        </Link>
      </div>
    </div>
  );
};

export default HeroGrid;
