"use client";

import Link from "next/link";
import SmallScreenNav from "./SmallScreenNav";
import { usePathname } from "next/navigation";

const Nav = () => {
  const pathname = usePathname();
  return (
    <>
      {/* Rendering the burger icon only on small screens */}
      <div className="block sm:hidden">
        <SmallScreenNav />
      </div>
      <div className="hidden sm:flex justify-between items-center px-8 py-4">
        <Link href="/">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="100"
            height="28"
            viewBox="0 0 100 28"
            fill="none"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M21.875 14.125H5.375V5.75L21.625 5.875C21.875 8.625 21.9583 11.375 21.875 14.125Z"
              fill="#FEA739"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M95 10.75L93.75 11C92.75 10.5 92.5 9.83334 93 9.00001C93.8333 8.33334 94.625 8.37501 95.375 9.12501C95.5417 9.87501 95.375 10.4167 94.875 10.75"
              fill="#1D1414"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M25.375 8.75V13.5H30.875V8.75H32.875V20.25H30.875L30.75 15.25C29.0833 15.0833 27.2917 15.0417 25.375 15.125V20.25H23.5V8.875L25.375 8.75Z"
              fill="#100101"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M91.75 19.75V20.375H89.625L88.625 18.125C86.9667 17.9163 85.2917 17.8744 83.625 18L82.375 20.5H80.125L80.25 20L85 8.75H87.125L91.75 19.75Z"
              fill="#160A0A"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M78 10.25V12.625H80V13.875H78L78.125 18.125C78.125 18.4583 78.25 18.7083 78.5 18.875H80V20.625C78.1667 21.2083 76.875 20.6667 76.125 19V14.125L74.875 14V12.5H76.125V10.25H78Z"
              fill="#130606"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M87.5 15.625C87.0833 15.875 86.5833 16 86 16C85.4167 16 84.875 15.9167 84.375 15.75C84.7917 14.25 85.375 12.8333 86.125 11.5C86.7917 12.8333 87.2917 14.2083 87.625 15.625"
              fill="#EEEDED"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M39.6249 12.5C41.0416 12.5 41.8749 13.25 42.1249 14.75V20.375H40.3749V19.75C40.2082 19.75 40.0832 19.8333 39.9999 20C39.7499 20.3333 39.4166 20.5417 38.9999 20.625L36.7499 20.5L35.4999 19.875C34.3332 18.375 34.5832 17.125 36.2499 16.125C36.9166 15.7917 37.4999 15.625 37.9999 15.625H40.3749L40.2499 14.875C39.9166 14.125 39.3332 13.8333 38.4999 14C37.6666 14 36.9582 14.25 36.3749 14.75L35.3749 13.5C35.7916 13 36.3332 12.7083 36.9999 12.625C37.8332 12.4583 38.7082 12.375 39.6249 12.375"
              fill="#150909"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M48 12.5C48.5833 12.3333 49 12.4583 49.25 12.875V14.125C47.8333 13.875 46.9583 14.5 46.625 16C46.375 17.4167 46.2917 18.875 46.375 20.375H44.625V12.625H46.375L46.5 13.625C46.9167 13.0417 47.4167 12.625 48 12.375"
              fill="#120303"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M66.3752 15V17H60.5002C60.5002 17.25 60.5835 17.4167 60.7502 17.5C61.3335 19 62.3752 19.4583 63.8752 18.875L65.0002 18.125L66.2502 19.125C65.4169 20.375 64.2502 20.9167 62.7502 20.75C60.5002 20.75 59.1252 19.6667 58.6252 17.5C58.2085 14.5 59.5418 12.75 62.6252 12.25C64.6252 12.25 65.8752 13.1667 66.3752 15Z"
              fill="#150808"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M70.3751 17.25C70.2333 17.0993 70.0563 16.9861 69.8599 16.9206C69.6636 16.8552 69.454 16.8395 69.2501 16.875C69.2501 16.625 69.0835 16.5 68.7501 16.5C67.5001 14.6667 67.9168 13.25 70.0001 12.25C71.2501 12.0833 72.4168 12.2917 73.5001 12.875L74.1251 13.5L73.0001 14.625L71.8751 13.875H70.3751C69.8751 14.375 69.8751 14.875 70.3751 15.375C71.7085 15.4583 72.8335 15.9583 73.7501 16.875C74.5835 18.7917 74.0418 20.0417 72.1251 20.625C70.7732 20.9047 69.3667 20.7289 68.1251 20.125C67.7085 19.9583 67.5835 19.625 67.7501 19.125L69.0001 18.25C69.2931 18.6154 69.6827 18.8911 70.1247 19.0458C70.5667 19.2005 71.0433 19.2279 71.5001 19.125C72.1668 18.625 72.2501 18.125 71.7501 17.625L70.3751 17.25Z"
              fill="#170B0B"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M49.5 12.875C50.0833 12.625 50.7917 12.5417 51.625 12.625L53.625 17.875L55.625 12.625C56.3573 12.3947 57.1427 12.3947 57.875 12.625C56.7083 15.125 55.7083 17.7917 54.875 20.625C54.0417 20.625 53.25 20.5417 52.5 20.375L49.5 12.875Z"
              fill="#170D0D"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M93.125 12.5H95.125L95.25 20.625H93.125V12.5Z"
              fill="#1B1212"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M64.5 15V15.625C63.0833 15.625 61.8333 15.5 60.75 15.25C61.3333 13.9167 62.3333 13.5 63.75 14C64.1667 14.3334 64.4167 14.6667 64.5 15Z"
              fill="#F5F4F4"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M13.75 14.125H21.875V22.5H13.625L13.75 14.125Z"
              fill="#07C299"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M40.2502 17.75C39.7502 19 38.8335 19.4167 37.5002 19C36.7502 18.3333 36.8752 17.75 37.8752 17.25L40.2502 17V17.75Z"
              fill="#F7F6F6"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M5.375 14.125H13.75V22.5H5.375V14.125Z"
              fill="#28738C"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M5.625 14.375H13.5V22.375H5.625V14.375Z"
              fill="#02658C"
            />
          </svg>
        </Link>
        <div className="space-x-16 transition">
          <Link href="/">Home</Link>
          <Link
            className={`hover:border-b hover:border-black ${
              pathname === "/weather" ? "border-b border-black" : ""
            }`}
            href="/weather"
          >
            Weather
          </Link>
          <Link
            className={`hover:border-b hover:border-black ${
              pathname === "/chart" ? "border-b border-black" : ""
            }`}
            href="/chart"
          >
            Charts
          </Link>
        </div>
      </div>
    </>
  );
};

export default Nav;
