import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  ResponsiveContainer,
  Cell,
} from "recharts";

// Recharts library was utilized to dynamically render the bar charts
// Note: The 'dataKey' props in each 'Line' component and in the 'XAxis' must align with the corresponding keys in the provided 'data' array
const BarChartComponent = ({ data }) => {
  const barColors = ["#03668d", "#00c39a", "#ffa737", "#0f0000"];
  return (
    <ResponsiveContainer width="100%" height="100%">
      <BarChart
        width={500}
        height={300}
        // Array of objects containing the 'barValues'
        data={data}
        margin={{
          right: 30,
        }}
      >
        <XAxis dataKey="value" />
        <YAxis domain={[0, 1000]} hide={true} />
        <Tooltip />
        <Legend />
        {/* Assigning a unique color for each bar */}
        <Bar dataKey="value" fill="8884d8">
          {data.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={barColors[index]} />
          ))}
        </Bar>
      </BarChart>
    </ResponsiveContainer>
  );
};

export default BarChartComponent;
