import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

// Recharts library was utilized to render the weather forecast in a line chart
// Note: The 'dataKey' props in each 'Line' component and in the 'XAxis' must align with the corresponding keys in the provided 'data' array
const LineChartComponent = ({ data }) => {
  return (
    <ResponsiveContainer width="100%" height="100%">
      <LineChart
        className="pl-2 pb-4"
        width={500}
        height={300}
        // Array of objects containing the min and max temperature values
        data={data}
        margin={{
          right: 30,
        }}
      >
        <XAxis
          dataKey="date"
          label={{ value: "Date", position: "insideBottom", offset: -10 }}
        />
        <YAxis
          label={{
            value: "Temperature (°C)",
            angle: -90,
            position: "insideLeft",
          }}
        />
        <Tooltip />
        <Legend />
        <Line
          type="monotone"
          dataKey="maxTemp"
          stroke="#e67e22"
          name="Max Temperature"
        />
        <Line
          type="monotone"
          dataKey="minTemp"
          stroke="#3498db"
          name="Min Temperature"
        />
      </LineChart>
    </ResponsiveContainer>
  );
};

export default LineChartComponent;
