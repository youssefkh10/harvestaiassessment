# HarvestAi Assessment

Welcome to the HarvestAi Assessment project repository! This project was created as part of the HarvestAi application process.

## Installation

1. Clone the repository: `git clone https://gitlab.com/youssefkh10/harvestaiassessment.git`
2. Navigate to the project directory: `cd harvestaiassessment`
3. Install dependencies: `npm install`

**Note:** Node.js and Git must to be installed on your device. If you don't have them installed, you can download them from the following links:

- Node.js: [Download Node.js](https://nodejs.org/)
- Git: [Download Git](https://git-scm.com/)

## Usage

To run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser.

## Weather API

- **Weather Data Source:**

  - The weather information for the next 7 days is sourced from Open-Meteo Weather API, as it was the most suitable free option available. Please note that due to the limited availability of free APIs, the accuracy of the weather data might not be 100%.

- **Temperature Line Charts:**

  - As the API supplies data for minimum and maximum temperatures for each day, two line charts were created for minimum and maximum temperatures.

  ## Example API Response

  Here is an example of the API response for the weather data:

  ```json
  {
    "latitude": 6.25,
    "longitude": 55.375,
    "generationtime_ms": 0.7150173187255859,
    "utc_offset_seconds": 0,
    "timezone": "GMT",
    "timezone_abbreviation": "GMT",
    "elevation": 0.0,
    "daily_units": {
      "time": "iso8601",
      "temperature_2m_max": "°C",
      "temperature_2m_min": "°C"
    },
    "daily": {
      "time": [
        "2024-01-15",
        "2024-01-16",
        "2024-01-17",
        "2024-01-18",
        "2024-01-19",
        "2024-01-20",
        "2024-01-21"
      ],
      "temperature_2m_max": [27.2, 27.7, 27.5, 27.0, 28.0, 27.6, 28.0],
      "temperature_2m_min": [26.5, 26.7, 26.5, 26.3, 26.4, 26.6, 26.9]
    }
  }
  ```

## Design Decisions

- **Background Color:**

  - The background color was chosen for its comfort on the eyes, providing a pleasant environment.

- **Cards with Hover Effect:**

  - The use of cards with a hover effect on the landing page was implemented to enhance the user experience, making the interface more interactive.

- **Bar Chart Colors:**
  - The colors of the bar charts were specifically chosen to align with the HarvestAi logo, maintaining a visual identity.

## Author

- Youssef Khaled
